package zelek.rafal.core.domain

import org.scalatest.Matchers

class GameBoardTest extends org.scalatest.FunSuite with Matchers {
  val gameBoard = GameBoard(3, 3)
  val testCases = (GameAreaPosition(0, 0), GameGridPosition(1, 1)) ::
    (GameAreaPosition(1, 1), GameGridPosition(11, 11)) ::
    (GameAreaPosition(1, 1), GameGridPosition(10, 10)) ::
    (GameAreaPosition(2, 1), GameGridPosition(29, 19)) ::
    Nil
  for {
    (expectedGameAreaPosition, gameGridPosition) <- testCases
  }
    test(s"gameAreaByGameGridPosition should return $expectedGameAreaPosition for $gameGridPosition") {
      val Some((gameAreaPosition, gameArea)) = gameBoard.gameAreaByGameGridPosition(gameGridPosition)
      gameAreaPosition shouldEqual expectedGameAreaPosition
    }
}
