package zelek.rafal.core.domain

import monocle.macros.Lenses

@Lenses("_") case class GameDominationPoint(numberOfPoints: Int)

object GameDominationPoint{
  implicit val ordering = new Ordering[GameDominationPoint] {
    override def compare(x: GameDominationPoint, y: GameDominationPoint): Int =
      Ordering[Int].compare(x.numberOfPoints, y.numberOfPoints)
  }
}

@Lenses("_") case class GameArea(width: Int, height: Int, points: Map[SnakeId, GameDominationPoint], food: Map[GameGridPosition, Food])

object GameArea {
  val defaultGameAreaWidth = 30
  val defaultGameAreaHeight = 20
}

@Lenses("_") case class GameAreaPosition(x: Int, y: Int)


@Lenses("_") case class GameBoard(
  numberOfGameAreasInWidth: Int,
  numberOfGameAreasInHeight: Int,
  gameAreas: Map[GameAreaPosition, GameArea]) {
  def score(snake: Snake): GameBoard = {
    //TODO: in theory there should not be a situation when a snake gets point outside any area, so it should be safe, but think about improvement
    val (gameAreaPosition, gameArea) = gameAreaByGameGridPosition(snake.bodyElements.head).get
    val gameDominationPoint = gameArea.points.getOrElse(snake.id, GameDominationPoint(0))
    //TODO: replace deep copy with lens library
    val newScore = gameDominationPoint.copy(numberOfPoints = gameDominationPoint.numberOfPoints + 1)
    copy(gameAreas = gameAreas + (gameAreaPosition -> gameArea.copy(points = gameArea.points + (snake.id -> newScore))))
  }

  def gameAreaByGameGridPosition(gameGridPosition: GameGridPosition): Option[(GameAreaPosition, GameArea)] = {
    val gameAreaPosition = GameAreaPosition(gameGridPosition.x/10, gameGridPosition.y/10)
    gameAreas.find(_._1 == gameAreaPosition)
  }
}

object GameBoard {
  def apply(numberOfGameAreasInWidth: Int, numberOfGameAreasInHeight: Int): GameBoard = {
    val gameAreas: Map[GameAreaPosition, GameArea] = {
      for {
        x <- 0 until numberOfGameAreasInWidth
        y <- 0 until numberOfGameAreasInHeight
      } yield GameAreaPosition(x, y) -> GameArea(GameArea.defaultGameAreaWidth, GameArea.defaultGameAreaHeight, Map.empty, Map.empty)
    }.toMap
    GameBoard(numberOfGameAreasInWidth, numberOfGameAreasInHeight, gameAreas)
  }
}