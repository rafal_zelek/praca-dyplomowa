package zelek.rafal.core.domain

import cats.monocle.syntax._
import com.badlogic.gdx.Gdx
import monocle.macros.Lenses
import zelek.rafal.core.domain.Snake._
import zelek.rafal.core.domain.GameArea._
import zelek.rafal.core.domain.GameBoard._
import monocle.function.all._
import scala.annotation.tailrec
import scala.util.Random
import cats.implicits._
import cats.syntax.semigroup._
import cats.data._

@Lenses("_") case class SnakeContext(snake: Snake, keySchema: KeySchema)

import SnakeContext._

@Lenses("_") case class SnakeGameState(
  snakes: List[SnakeContext],
  gameBoard: GameBoard,
  initialCountdownSeconds: Float,
  accumulatedDeltaTime: Float
) {
  def timeToStart: Float = Math.max(0f, initialCountdownSeconds - accumulatedDeltaTime)

  def hasGameStarted: Boolean = timeToStart == 0f
}

object SnakeGameState {
  type StateSGS[A] = State[SnakeGameState, A]
  type StateSnake[A] = State[Snake, A]

  def logic(initialSnake: Snake, initialSnake2: Snake)(deltaTime: Float): State[SnakeGameState, Unit] =
    for {
      _ <- _accumulatedDeltaTime %%= (_ + deltaTime)
      _ <- generateFood
      hasGameStarted <- State.inspect[SnakeGameState, Boolean](_.hasGameStarted)
      _ <- modifyIf(hasGameStarted)((_snakes composeTraversal each) %%= movementLogicSnake(deltaTime))
      _ <- eatFood
      killIfOutside <- (_accumulatedDeltaTime.toState, _gameBoard.toState).mapN(killSnakeIfHeIsOutsideTheBoard _)
      _ <- (_snakes composeTraversal each composeLens _snake) %%= killIfOutside
      accumulatedDeltaTime <- _accumulatedDeltaTime.toState
      _ <- (_snakes composeTraversal each composeLens _snake) %%= killSnakeIfHeIsEatingHimself(accumulatedDeltaTime)
      _ <- (_snakes composeTraversal each composeLens _snake) %%= resetSnakeState(initialSnake)
    } yield {}

  private def resetSnakeState(initialSnake: Snake) = {
    for {
      isAlive <- _isAlive.toState
      _ <- modifyIf(!isAlive)(State.modify[Snake](_ => initialSnake))
    } yield ()
  }

  def killSnakeIfHeIsEatingHimself(accumulatedDeltaTime: Float): State[Snake, Unit] = for {
    body <- State.inspect[Snake, List[GameGridPosition]](_.bodyElements)
    isHeadCollidingWithTail = body.tail.contains(body.head)
    _ <- modifyIf(isHeadCollidingWithTail)(Snake.dead(accumulatedDeltaTime))
  } yield {}

  def killSnakeIfHeIsOutsideTheBoard(accumulatedDeltaTime: Float, gameBoard: GameBoard): State[Snake, Unit] = for {
    head <- State.inspect[Snake, GameGridPosition](_.bodyElements.head)
    _ <- modifyIf(isSnakeOutsideTheBoard(gameBoard, head))(Snake.dead(accumulatedDeltaTime))
  } yield ()

  private def isSnakeOutsideTheBoard(gameBoard: GameBoard, head: GameGridPosition) = {
    head.x < 0 || head.x >= gameBoard.numberOfGameAreasInWidth * GameArea.defaultGameAreaWidth ||
      head.y < 0 || head.y >= gameBoard.numberOfGameAreasInHeight * GameArea.defaultGameAreaHeight
  }

  import monocle.unsafe.MapTraversal._

  val eatFood: State[SnakeGameState, Unit] = {
    for {
      allFoodOnTheMap <- _gameBoard composeLens _gameAreas inspect (_.values.flatMap(_.food.keys).toList)
      allWhichAreEatenBySnake <- _snakes composeTraversal each composeLens _snake %%={
        for{
          allWhichAreEatenBySnake <- _bodyElements inspect(be=>allFoodOnTheMap.filter(_ == be.head).toSet[GameGridPosition])
          _ <- _notProcessedEatenPoints %%= (_ + allWhichAreEatenBySnake.size)
        } yield ()
      }
      _ <- _gameBoard composeLens _gameAreas composeTraversal each composeLens _food %%= { food =>
        val positionToFood: Map[GameGridPosition, Food] = food.filterNot(f => allWhichAreEatenBySnake.flatMap(_.bodyElements).contains(f._1))
        positionToFood
      }
    } yield ()
  }

  private def movementLogicSnake(deltaTime: Float): State[SnakeContext, Unit] = {
    for {
      _ <- _snake composeLens _movementDelta %%= (_ + deltaTime)
      _isMovePossible <- _snake %%= isMovePossible
      keySchema <- _keySchema.toState
      _ <- modifyIfDefined(inputToMovingDirection(keySchema).filter(_isMovePossible)) { nextPotentialMove =>
        _snake composeLens _nextPotentialMove := nextPotentialMove
      }
      _ <- _snake %%= onMovement _
    } yield {}
  }

  @tailrec
  def onMovement(snake: Snake): Snake = {
    val movementDelta = 1f / snake.movementsPerSecond
    if (snake.movementDelta >= movementDelta) {
      val newDelta = snake.movementDelta - movementDelta
      val newState = Snake.move.run(snake.copy(
        movingDirection = snake.nextPotentialMove,
        movementDelta = newDelta
      )).value._1
      onMovement(newState)
    } else {
      snake
    }
  }

  val generateFood: State[SnakeGameState, Map[GameAreaPosition, GameArea]] = _gameBoard composeLens _gameAreas %%= (ga => ga ++ generateFoodOnAreas(ga))

  def inputToMovingDirection(keySchema: KeySchema): Option[MovingDirection] = {
    if (Gdx.input.isKeyPressed(keySchema.left)) {
      Some(MovingDirection.Left)
    } else if (Gdx.input.isKeyPressed(keySchema.right)) {
      Some(MovingDirection.Right)
    } else if (Gdx.input.isKeyPressed(keySchema.up)) {
      Some(MovingDirection.Up)
    } else if (Gdx.input.isKeyPressed(keySchema.down)) {
      Some(MovingDirection.Down)
    } else {
      None
    }
  }

  //TODO: improve food generation, to not generate on snake.
  // idea: create a function which returns "availble" fields
  def generateFoodOnAreas(gameAreas: Map[GameAreaPosition, GameArea]): Map[GameAreaPosition, GameArea] = {
    gameAreas.filter(_._2.food.isEmpty).map {
      case (gameAreaPosition, gameArea) =>
        val leftBound = gameAreaPosition.x * gameArea.width
        val rightBound = gameAreaPosition.x * gameArea.width + gameArea.width
        val upperBound = gameAreaPosition.y * gameArea.height + gameArea.height
        val lowerBound = gameAreaPosition.y * gameArea.height
        val random = new Random()
        (gameAreaPosition,
          gameArea.copy(
            food = gameArea.food +
              (GameGridPosition(leftBound + random.nextInt(rightBound - leftBound) + 1,
                lowerBound + random.nextInt(upperBound - lowerBound) + 1) -> Food()
                )
          )
        )
    }
  }


  private def modifyIf[S, R](condition: Boolean)(f: => State[S, R]): State[S, Unit] = if (condition) {
    f.map(_ => ())
  } else {
    State.inspect(_ => ())
  }

  private def modifyIfDefined[S, R, A](optionValue: Option[A])(f: A => State[S, R]): State[S, Unit] = if (optionValue.isDefined) {
    f(optionValue.get).map(_ => ())
  } else {
    State.inspect(_ => ())
  }
}