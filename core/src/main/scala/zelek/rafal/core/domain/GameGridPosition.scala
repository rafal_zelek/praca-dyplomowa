package zelek.rafal.core.domain

case class GameGridPosition(x: Int, y: Int) {
  def left = GameGridPosition(x - 1, y)

  def right = GameGridPosition(x + 1, y)

  def up = GameGridPosition(x, y + 1)

  def down = GameGridPosition(x, y - 1)
}

//TODO: add different position builders to prevent positionBuilder.left.right etc.
case class PositionBuilder(positions: List[GameGridPosition]) {
  private def move(f: GameGridPosition => GameGridPosition): List[GameGridPosition] = f(positions.head) :: positions

  def left: PositionBuilder = PositionBuilder(move(_.left))

  def right: PositionBuilder = PositionBuilder(move(_.right))

  def up: PositionBuilder = PositionBuilder(move(_.up))

  def down: PositionBuilder = PositionBuilder(move(_.down))

  def build: List[GameGridPosition] = positions.reverse
}

object PositionBuilder {
  def apply(position: GameGridPosition): PositionBuilder = PositionBuilder(List(position))
}