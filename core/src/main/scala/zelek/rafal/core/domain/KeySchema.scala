package zelek.rafal.core.domain

import com.badlogic.gdx.Input

trait KeySchema {
  val left: Int
  val right: Int
  val up: Int
  val down: Int
}


object WSADKeySchema extends KeySchema {
  override val left: Int = Input.Keys.A
  override val right: Int = Input.Keys.D
  override val up: Int = Input.Keys.W
  override val down: Int = Input.Keys.S
}


object ArrowsKeySchema extends KeySchema {
  override val left: Int = Input.Keys.LEFT
  override val right: Int = Input.Keys.RIGHT
  override val up: Int = Input.Keys.UP
  override val down: Int = Input.Keys.DOWN
}
