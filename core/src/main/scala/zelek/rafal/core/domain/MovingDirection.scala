package zelek.rafal.core.domain

trait MovingDirection

object MovingDirection {

  case object Left extends MovingDirection

  case object Right extends MovingDirection

  case object Up extends MovingDirection

  case object Down extends MovingDirection

}