package zelek.rafal.core.domain

import java.util.UUID

import cats.data.State
import cats.monocle.syntax._
import monocle.macros.Lenses
import zelek.rafal.core.domain.MovingDirection.{Down, Left, Right, Up}

case class SnakeId(id: UUID) extends AnyVal

object SnakeId {
  def random() = SnakeId(UUID.randomUUID())
}

object Snake {
  val decreaseNotProcessedEatenPoints: Int => Int = notProcessedEatenPoints => Math.max(0, notProcessedEatenPoints - 1)

  def move: State[Snake, Unit] = for {
    newHeadPositionFunc <- _movingDirection inspect newSnakeHeadPosition
    notProcessedEatenPoints <- _notProcessedEatenPoints.toState
    _ <- _bodyElements %%= (bodyElements => moveBody(notProcessedEatenPoints, newHeadPositionFunc(bodyElements.head), bodyElements))
    _ <- _notProcessedEatenPoints %%= decreaseNotProcessedEatenPoints
  } yield {}


  def isMovePossible: State[Snake, MovingDirection => Boolean] = for {
    pd <- _movingDirection.toState
  } yield (nextPotentialMove: MovingDirection) => PossibleMoves.fromMovingDirection(pd).moves.contains(nextPotentialMove)

  def dead(deadTime: Float): State[Snake, Unit] = for {
    _ <- _isAlive := false
    _ <- _deadTime := deadTime
  } yield {}


  def moveBody(notProcessedEatenPoints: Int, newHeadPosition: GameGridPosition, bodyElements: List[GameGridPosition]): List[GameGridPosition] = {
    val newPositions = newHeadPosition :: bodyElements
    if (notProcessedEatenPoints > 0)
      newPositions
    else
      newPositions.dropRight(1)
  }

  def newSnakeHeadPosition(movingDirection: MovingDirection)(gameGridPosition: GameGridPosition): GameGridPosition = {
    movingDirection match {
      case Left => gameGridPosition.left
      case Right => gameGridPosition.right
      case Up => gameGridPosition.up
      case Down => gameGridPosition.down
    }
  }
}

@Lenses("_") case class Snake(
  id: SnakeId,
  bodyElements: List[GameGridPosition],
  movingDirection: MovingDirection,
  isAlive: Boolean,
  deadTime: Float,
  movementsPerSecond: Int,
  movementDelta: Float,
  nextPotentialMove: MovingDirection,
  notProcessedEatenPoints: Int)


case class PossibleMoves(moves: List[MovingDirection]) {
  def isMovePossible(move: MovingDirection): Boolean = moves.contains(move)
}

object PossibleMoves {
  def apply(moves: MovingDirection*): PossibleMoves = PossibleMoves(moves.toList)

  def fromMovingDirection(movingDirection: MovingDirection): PossibleMoves = {
    movingDirection match {
      case Left => forLeftMovingDirection
      case Right => forRightMovingDirection
      case Up => forUpMovingDirection
      case Down => forDownMovingDirection
    }
  }

  val forLeftMovingDirection: PossibleMoves = PossibleMoves(Up, Down)
  val forRightMovingDirection: PossibleMoves = forLeftMovingDirection
  val forUpMovingDirection: PossibleMoves = PossibleMoves(Left, Right)
  val forDownMovingDirection: PossibleMoves = forUpMovingDirection
}
