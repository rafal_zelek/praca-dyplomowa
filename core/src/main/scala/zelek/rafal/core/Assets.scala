package zelek.rafal.core

object Assets {

  object Snake {

    object Body {
      val DOWN_LEFT = "snake/body/down-left.png"
      val DOWN_RIGHT = "snake/body/down-right.png"
      val LEFT_RIGHT = "snake/body/left-right.png"
      val UP_DOWN = "snake/body/up-down.png"
      val UP_LEFT = "snake/body/up-left.png"
      val UP_RIGHT = "snake/body/up-right.png"
    }

    object Head {
      val DOWN = "snake/head/down.png"
      val LEFT = "snake/head/left.png"
      val RIGHT = "snake/head/right.png"
      val UP = "snake/head/up.png"
    }

    object Tail {
      val DOWN = "snake/tail/down.png"
      val LEFT = "snake/tail/left.png"
      val RIGHT = "snake/tail/right.png"
      val UP = "snake/tail/up.png"
    }

  }

  object Food {
    val APPLE = "food/apple.png"
  }

  val ALL: List[String] =
    Snake.Body.DOWN_LEFT ::
      Snake.Body.DOWN_RIGHT ::
      Snake.Body.LEFT_RIGHT ::
      Snake.Body.UP_DOWN ::
      Snake.Body.UP_LEFT ::
      Snake.Body.UP_RIGHT ::
      Snake.Head.DOWN ::
      Snake.Head.LEFT ::
      Snake.Head.RIGHT ::
      Snake.Head.UP ::
      Snake.Tail.DOWN ::
      Snake.Tail.LEFT ::
      Snake.Tail.RIGHT ::
      Snake.Tail.UP ::
      Food.APPLE ::
      Nil
}