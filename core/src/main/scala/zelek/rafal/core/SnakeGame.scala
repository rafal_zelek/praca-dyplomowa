package zelek.rafal.core

import cats.data.State
import com.badlogic.gdx.assets.{AssetDescriptor, AssetErrorListener, AssetManager}
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.utils.viewport.{ExtendViewport, Viewport}
import com.badlogic.gdx.{Game, Gdx, Input, Screen}
import zelek.rafal.core.domain._

import scala.annotation.tailrec
import scala.util.Try

class SnakeGame extends Game {
  override def create() = {
    setScreen(new MyGameScreen)
  }
}


class MyGameScreen extends Screen {
  val batch = new SpriteBatch()
  val font = new BitmapFont()
  val rotationSpeed = 0.5f

  val WORLD_WIDTH = 100
  val WORLD_HEIGHT = 100

  var viewport: Viewport = _

  private val GRID_SIZE = 32
  private val VIEWPORT_WIDTH = 800
  private val VIEWPORT_HEIGHT = 1200

  var camera: OrthographicCamera = {
    val cam = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)
    viewport = new ExtendViewport(GameArea.defaultGameAreaWidth * GRID_SIZE, GameArea.defaultGameAreaHeight * GRID_SIZE, cam)
    viewport.apply()
    //    cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0)
    //    cam.zoom = 18f
    cam.update()
    cam
  }

  val manager: AssetManager = {
    //    val m = new AssetManager(new ClasspathFileHandleResolver())
    val m = new AssetManager()
    m.setErrorListener(new AssetErrorListener {
      override def error(asset: AssetDescriptor[_], throwable: Throwable): Unit = println(throwable)
    })
    Assets.ALL.foreach(asset => m.load(asset, classOf[Texture]))
    m.finishLoading()
    m
  }

  private val snake = Snake(
    SnakeId.random(),
    PositionBuilder(GameGridPosition(1, 1)).up.up.up.up.build,
    MovingDirection.Right,
    isAlive = true,
    movementsPerSecond = 5,
    movementDelta = 0f,
    nextPotentialMove = MovingDirection.Right,
    notProcessedEatenPoints = 0,
    deadTime = 0f
  )

  private val snake2 = Snake(
    SnakeId.random(),
    PositionBuilder(GameGridPosition(3, 3)).up.up.up.up.build,
    MovingDirection.Right,
    isAlive = true,
    movementsPerSecond = 1,
    movementDelta = 0f,
    nextPotentialMove = MovingDirection.Right,
    notProcessedEatenPoints = 0,
    deadTime = 0f
  )

  var snakeGameState = SnakeGameState(
    List(SnakeContext(snake, WSADKeySchema),
      SnakeContext(snake2, ArrowsKeySchema)),
    GameBoard(numberOfGameAreasInWidth = 3, numberOfGameAreasInHeight = 3),
    initialCountdownSeconds = 5f,
    accumulatedDeltaTime = 0f
  )

  val logic: Float => State[SnakeGameState, Unit] = SnakeGameState.logic(snake, snake2)

  override def render(deltaTime: Float): Unit = {
    snakeGameState = time(logic(deltaTime).run(snakeGameState).value._1)
    Gdx.graphics.getGL20.glClear(GL20.GL_COLOR_BUFFER_BIT)
    //        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth / 2 - 20, Gdx.graphics.getHeight)
    camera.position.set(snakeGameState.snakes.head.snake.bodyElements.head.x * GRID_SIZE,
      snakeGameState.snakes.head.snake.bodyElements.head.y * GRID_SIZE, 0)
    camera.update()
    batch.setProjectionMatrix(camera.combined)
    batch.begin()
    manager.update()
    renderGameBoard(snakeGameState.gameBoard, batch)
    renderFood(snakeGameState.gameBoard, batch)
    snakeGameState.snakes.map(_.snake).foreach(snake => renderSnake(snake, batch))
    renderGui(snakeGameState, batch)
    batch.end()
    if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
      Gdx.app.exit()
  }

  val colors = Map(
    GameAreaPosition(0, 0) -> Color.BLUE,
    GameAreaPosition(0, 1) -> Color.RED,
    GameAreaPosition(0, 2) -> Color.YELLOW,
    GameAreaPosition(1, 0) -> Color.GOLD,
    GameAreaPosition(1, 1) -> Color.GRAY,
    GameAreaPosition(1, 2) -> Color.GREEN,
    GameAreaPosition(2, 0) -> Color.ROYAL,
    GameAreaPosition(2, 1) -> Color.FIREBRICK,
    GameAreaPosition(2, 2) -> Color.CHARTREUSE
  )
  val shapeRenderer = new ShapeRenderer()

  def renderGameBoard(gameBoard: GameBoard, spriteBatch: SpriteBatch): Unit = {
    spriteBatch.end()
    gameBoard.gameAreas.foreach {
      case (gameAreaPosition, gameArea) =>
        val leftBound = gameAreaPosition.x * gameArea.width
        val lowerBound = gameAreaPosition.y * gameArea.height
        shapeRenderer.setProjectionMatrix(spriteBatch.getProjectionMatrix)
        shapeRenderer.begin(ShapeType.Filled)
        val maxScore = Try(gameArea.points.toSeq.maxBy(_._2))
        //        val areaColor = if (maxScore.isFailure)
        val areaColor = colors(gameAreaPosition) //Color.WHITE
        //        else
        //          snakeIdToColor(maxScore.get._1)
        shapeRenderer.setColor(areaColor)
        shapeRenderer.rect(leftBound * GRID_SIZE, lowerBound * GRID_SIZE, gameArea.width * GRID_SIZE, gameArea.height * GRID_SIZE)
        shapeRenderer.end()
    }
    spriteBatch.begin()
  }

  private def renderFood(gameBoard: GameBoard, spriteBatch: SpriteBatch): Unit = {
    gameBoard.gameAreas.foreach {
      case (gameAreaPosition, gameArea) =>
        gameArea.food.foreach {
          case (gameGridPosition, food) =>
            spriteBatch.draw(manager.get(Assets.Food.APPLE, classOf[Texture]), gameGridPosition.x * GRID_SIZE, gameGridPosition.y * GRID_SIZE)
        }
    }
  }

  def renderSnake(snake: Snake, spriteBatch: SpriteBatch): Unit = {
    gameGridPositionsToSnakeTextureName(snake.bodyElements).reverse.foreach {
      case (position, texture) =>
        spriteBatch.draw(manager.get(texture, classOf[Texture]), position.x * GRID_SIZE, position.y * GRID_SIZE)
    }
    import com.badlogic.gdx.graphics.Texture.TextureFilter
    font.getRegion.getTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear)
    font.getData.setScale(2)
    font.draw(batch, snake.id.toString, snake.bodyElements.head.x * GRID_SIZE, snake.bodyElements.head.y * GRID_SIZE)
  }

  def renderGui(snakeGameState: SnakeGameState, spriteBatch: SpriteBatch): Unit = {
    font.getRegion.getTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear)
    font.getData.setScale(1)
    if (!snakeGameState.hasGameStarted) {
      font.draw(batch, f"Game starts in: ${snakeGameState.timeToStart}%1.0f", 10, 10)
    }
  }

  override def dispose() = {
    batch.dispose()
    font.dispose()
    manager.dispose()
  }

  override def resize(width: Int, height: Int) = {
    println("resize")
    viewport.update(width, height)
    camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0)
  }

  override def hide() = {
    println("hide")
  }

  override def show() = {

    println("show")
  }

  override def pause() = {
    println("paused")
  }

  override def resume() = {
    println("resumed")
  }

  def gameGridPositionsToSnakeTextureName(positions: List[GameGridPosition]): List[(GameGridPosition, String)] = {
    @tailrec
    def helper(ps: List[GameGridPosition], prev: Option[GameGridPosition], acc: List[(GameGridPosition, String)]): List[(GameGridPosition, String)] =
      (ps, prev) match {
        //None means that there wasn't previous element and It's a snake's head
        case (current :: next :: _, None) if current.down == next => helper(ps.tail, Some(current), (current, Assets.Snake.Head.DOWN) :: acc)
        case (current :: next :: _, None) if current.up == next => helper(ps.tail, Some(current), (current, Assets.Snake.Head.UP) :: acc)
        case (current :: next :: _, None) if current.left == next => helper(ps.tail, Some(current), (current, Assets.Snake.Head.LEFT) :: acc)
        case (current :: next :: _, None) if current.right == next => helper(ps.tail, Some(current), (current, Assets.Snake.Head.RIGHT) :: acc)
        //Nil means that there is no more values, so it's just a snake's tail
        case (current :: Nil, Some(pr)) if current.down == pr => (current, Assets.Snake.Tail.DOWN) :: acc
        case (current :: Nil, Some(pr)) if current.up == pr => (current, Assets.Snake.Tail.UP) :: acc
        case (current :: Nil, Some(pr)) if current.left == pr => (current, Assets.Snake.Tail.LEFT) :: acc
        case (current :: Nil, Some(pr)) if current.right == pr => (current, Assets.Snake.Tail.RIGHT) :: acc
        //Some means it's snake's body
        case (current :: next :: _, Some(pr)) if (current.up == pr && current.down == next) || (current.up == next && current.down == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.UP_DOWN) :: acc)
        case (current :: next :: _, Some(pr)) if (current.down == pr && current.left == next) || (current.down == next && current.left == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.DOWN_LEFT) :: acc)
        case (current :: next :: _, Some(pr)) if (current.down == pr && current.right == next) || (current.down == next && current.right == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.DOWN_RIGHT) :: acc)
        case (current :: next :: _, Some(pr)) if (current.left == pr && current.right == next) || (current.left == next && current.right == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.LEFT_RIGHT) :: acc)
        case (current :: next :: _, Some(pr)) if (current.up == pr && current.left == next) || (current.up == next && current.left == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.UP_LEFT) :: acc)
        case (current :: next :: _, Some(pr)) if (current.up == pr && current.right == next) || (current.up == next && current.right == pr) =>
          helper(ps.tail, Some(current), (current, Assets.Snake.Body.UP_RIGHT) :: acc)
        case other =>
          val str = s"\n$ps\n$prev\n$acc\n${other._1.head.left}\n${other._1.head.right}\n${other._1.head.up}\n${other._1.head.down}\n"
          println(str)
          throw new RuntimeException()
      }

    helper(positions, None, List.empty).reverse
  }
}
