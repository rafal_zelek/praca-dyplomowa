package zelek.rafal

import android.os.Bundle
import com.badlogic.gdx.backends.android._
import zelek.rafal.core.SnakeGame

class SnakeGameActivity extends AndroidApplication {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    val cfg = new AndroidApplicationConfiguration
    initialize(new SnakeGame, cfg)
  }
}
