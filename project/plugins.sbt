resolvers += Resolver.sonatypeRepo("snapshots")

addSbtPlugin("org.lyranthe.sbt" % "partial-unification" % "1.1.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.0")

addSbtPlugin("com.hanhuy.sbt" % "android-sdk-plugin" % "1.5.20")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.0")