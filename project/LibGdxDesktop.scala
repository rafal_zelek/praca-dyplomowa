import LibGdxPlugin.{desktopDependency, gdxDependency}
import com.typesafe.sbt.SbtNativePackager.Universal
import com.typesafe.sbt.packager.archetypes.JavaAppPackaging
import sbt.Keys._
import sbt._

object LibGdxDesktop extends AutoPlugin {

  import LibGdxPlugin.autoImport._

  override def requires = LibGdxPlugin && JavaAppPackaging

  override def projectSettings = LibGdxPlugin.projectSettings ++
    baseProjectSettings

  lazy val baseProjectSettings: Seq[Def.Setting[_]] = Seq(
    fork in run := true,
    libraryDependencies ++= Seq(
      gdxDependency("gdx-backend-lwjgl").value,
      desktopDependency("gdx-platform").value
    ),
    mappings in Universal <++= assetMappingsTask
  )

  lazy val assetMappingsTask = Def.task {
    val dir = assetDir.value
    dir.*** pair rebase(dir, "bin/")
  }
}
