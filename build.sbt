val projectName = """libgdx-scala-seed"""

name := projectName

scalaVersion in ThisBuild := "2.11.8"

val sharedSettings = Seq(
  libGdxVersion := "1.9.2",
  version := "1.0.0",
  javacOptions ++= Seq("-source", "1.7", "-target", "1.7"),
  scalacOptions ++= Seq("-target:jvm-1.7", "-deprecation", "-feature")
)
val monocleVersion = "1.5.0-cats" // 1.5.0-cats based on cats 1.0.x

lazy val root = project.in(file("."))
  .aggregate(android, desktop)

lazy val core = project.in(file("core"))
  .settings(sharedSettings: _*)
  .settings(addCompilerPlugin("org.scalamacros" %% "paradise" % "2.1.0" cross CrossVersion.full))
  .settings(
    name := projectName + "-core",
    libraryDependencies ++= Seq(
      libGdx.value,
      "com.dragishak" %% "monocle-cats" % "1.4-SNAPSHOT",
      "com.github.julien-truffaut" %% "monocle-core" % monocleVersion,
      "com.github.julien-truffaut" %% "monocle-macro" % monocleVersion,
      "com.github.julien-truffaut"  %%  "monocle-unsafe"  % monocleVersion,
      "com.github.julien-truffaut" %% "monocle-law" % monocleVersion % "test",
      "org.typelevel" %% "cats-core" % "1.0.1",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test),
    exportJars := true
  )

lazy val android = project.in(file("android"))
  .settings(sharedSettings: _*)
  .settings(
    name := projectName + "-android",
    platformTarget in Android := "android-23",
    debugIncludesTests := true
  )
  .dependsOn(core)
  .enablePlugins(LibGdxAndroid)

lazy val desktop = project.in(file("desktop"))
  .settings(sharedSettings: _*)
  .settings(
    name := projectName + "-desktop",
    watchSources <++= sources in(core, Compile)
  )
  .dependsOn(core)
  .enablePlugins(LibGdxDesktop)

// Uncomment to generate a ".RUNNING_SBT" file when using desktop/run.
// This file will contain the path of the project directory.
// It can be detected using Gdx.files.internal(".RUNNING_SBT").exists
// This is useful for automatically enabling 'dev tools' when running.

// resourceGenerators in (desktop, Compile) += Def.task {
//   val file = baseDirectory.value / "desktop" / ".RUNNING_SBT"
//   IO.write(file, sys.props("user.dir"))
//   Seq(file)
// }.taskValue
