package zelek.rafal

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import zelek.rafal.core.SnakeGame

object SnakeDesktopApplication {
  def main(args: Array[String]) {
    val cfg = new LwjglApplicationConfiguration
    cfg.width = 1200
    cfg.height = 800
    new LwjglApplication(new SnakeGame, cfg)
  }
}
